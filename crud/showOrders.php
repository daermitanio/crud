<?php 
require "header.php";
require "model.php";

$model = new Model;
session_start();

$userOrders = $model->getDataById("orders",$_SESSION["user_id"],"user_id");

$orders = [];
$toppings = [];
$total = 0;

foreach($userOrders as $userOrder){
    $orders[] = $model->getJoinedTable($userOrder["order_id"]);
}
foreach($orders as $index => $order){?>
        <h2>Order Number <?= $index+1; ?></h2>
        <table>
            <tr>
                <th>Toppings Name</th>
                <th>Multiplier</th>
                <th>Price</th> 
                <th>SubTotal</th>
            </tr>

    <?php
    foreach($order as $orderList){
        ?>
        
        <tr>
            <td><?= $orderList["name"] ?></td>
            <td><?= $orderList["multiplier"] ?></td>
            <td><?= $orderList["price"] ?></td>
            <td><?= $orderList["price"] * $orderList["multiplier"] ?></td>
            <?php $total +=$orderList["price"] * $orderList["multiplier"]; ?>
        </tr>    

        <?php
    }
    ?>
       
        </table>
        <h3>Total:<?= $total; ?></h3>
        <?php $total = 0;?>
    <?php
}
?>

<a href="index.php">Back to Index page</a>
<?php require "footer.php" ?>