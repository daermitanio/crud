<?php

/**
 * Function to query information based on 
 * a parameter: in this case, user_id.
 *
 */
if (isset($_POST['submit'])) {
    try  {
        require "config.php";

        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT * 
                        FROM users
                        WHERE user_id = :user_id";

        $user_id = $_POST['user_id'];

        $statement = $connection->prepare($sql);
        $statement->bindParam(':user_id', $user_id, PDO::PARAM_STR);
        $statement->execute();

        $result = $statement->fetchAll();
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }
}
?>
<?php require "header.php"; ?>
        
<?php  
if (isset($_POST['submit'])) {
    if ($result && $statement->rowCount() > 0) { ?>
        <h2>Results</h2>

        <table>
            <thead>
                <tr>
                    <th>id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email Address</th>
                    <th>Address</th>
                    <th>Contact</th>
                </tr>
            </thead>
            <tbody>
        <?php foreach ($result as $row) { ?>
            <tr>
                <td><?php echo ($row["user_id"]); ?></td>
                <td><?php echo ($row["first_name"]); ?></td>
                <td><?php echo ($row["last_name"]); ?></td>
                <td><?php echo ($row["email"]); ?></td>
                <td><?php echo ($row["address"]); ?></td>
                <td><?php echo ($row["contact"]); ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <?php } else { ?>
        <blockquote>No results found for <?php echo ($_POST['user_id']); ?>.</blockquote>
    <?php } 
} ?> 

<h2>Find user based on user_id</h2>

<form method="post">
    <label for="location">User ID</label>
    <input type="text" id="user_id" name="user_id">
    <input type="submit" name="submit" value="View Results">
</form>

<a href="index.php">Back to home</a>

<?php require "footer.php"; ?>
