<?php require "header.php";?>
<?php require "model.php";?>

<?php 

$model = new Model;
$categories = $model->getData("categories");
// print_r($categories); die;
if(isset($_POST["add"])){
    $data = [
        "name" => ucfirst($_POST["name"]),
        "price" => $_POST["price"],
        "category_id" => $_POST["category_id"]
    ];
    array_map('ucfirst', $data);
    $model->insertData("toppings",$data);
}

?>

<form action="" method = "POST">
    <label for="name">Name:</label>
    <input type="text" id="name" name="name" required>
    <label for="price">Price:</label>
    <input type="text" name="price" id="price" required>
    <label for="categories">Categories</label>
    <select id="categories" name = "category_id">
        <?php  foreach($categories as $category): ?> 
        
        <option value = "<?= $category["category_id"]?>"><?= $category["category_name"]?> </option>
        
        <?php  endforeach; ?>
    </select>
    <br><br>
    <input type = "submit" value = "Add" name = "add">
       
</form>


<a href="index.php">Back to Index page</a>

<?php require "footer.php" ?>