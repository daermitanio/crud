<?php

/**
 * Use an HTML form to create a new entry in the
 * users table.
 *
 */

$statement = false;
if (isset($_POST['submit'])) {
    require "config.php";
    // session_start();
    
    if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $error = "Invalid email format"; 
    }
    else{
        try  {
            $connection = new PDO($dsn, $username, $password, $options);
            
            $new_user = array(
                "first_name" => $_POST['first_name'],
                "last_name"  => $_POST['last_name'],
                "email"  => $_POST['email'],
                "address"     => $_POST['address'],
                "contact"       => $_POST['contact'],
                "password" => md5($_POST['password']),
                "created_at"  => date('Y-m-d H:i:s'),
                "updated_at"  => date('Y-m-d H:i:s')
            );

            $sql = sprintf(
                    "INSERT INTO %s (%s) values (%s)",
                    "users",
                    implode(", ", array_keys($new_user)),
                    ":" . implode(", :", array_keys($new_user))
            );
            $statement = $connection->prepare($sql);
            $statement->execute($new_user);

        } catch(PDOException $error) {
            echo $sql . "<br>" . $error->getMessage();
        }
    }
}


?>

<?php require "header.php"; ?>

<?php if (isset($_POST['submit']) && $statement) { ?>
     ?>
        <blockquote><?php echo "{$_POST['first_name']} {$_POST['last_name']}"; ?> Customer was successfully added.</blockquote>
    <?php 
    }
    if(!empty($error)){
    ?>
        <p><?= $error ?></p>
    <?php
    }
 ?>

<h2>Add a Customer</h2>

<form method="post">
    <label for="first_name">First Name</label>
    <input type="text" name="first_name" id="first_name">
    <label for="last_name">Last Name</label>
    <input type="text" name="last_name" id="last_name">
    <label for="email">Email Address</label>
    <input type="text" name="email" id="email">
    <label for="address">Address</label>
    <input type="text" name="address" id="address">
    <label for="contact">Contact</label>
    <input type="text" name="contact" id="contact">
    <label for="pass">Password</label>
    <input type="password" name="password" id="pass">
    <input type="submit" name="submit" value="Submit">
</form>

<a href="index.php">Back to Index page</a>

<?php require "footer.php"; ?>
