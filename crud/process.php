<?php 
require_once "model.php";
class process{

    public function computeTotalOrder($orders){
        $total = 0.0;
        $multiplier = 1;
        $model = new Model;
        session_start();
        $sizes = $model->getDataById("sizes",$orders["sizes"],"size_id");    
        $categories = $model->getData("categories");    
        $toppings;

        $User = [
            "user_id" => $_SESSION["user_id"],
            "created_at"  => date('Y-m-d H:i:s')
        ];
        $order_id = $model->insertData("orders",$User);


        foreach($orders["toppings_id"] as $toppings_id){
            $toppings[] = $model->getDataById("toppings", $toppings_id,"topping_id");                
        }
        
        foreach($toppings as $topping){
            $total += floatval($topping[0]["price"]);

            $orderArray = [
                "order_id" => $order_id,
                "topping_id" => $topping[0]["topping_id"],
                "price" => $topping[0]["price"],
                "multiplier" => $sizes[0]["value"],
                "sub_total" => $sizes[0]["value"]*$topping[0]["price"],
                "created_at"  => date('Y-m-d H:i:s')
            ];
            $model->insertData("order_details",$orderArray);
        }

        $total *= $sizes[0]["value"];

        $returnValue = [
            "total" => $total,
            "sizes" => $sizes,
            "toppings" => $toppings
        ];

        return $returnValue;
        
    }

}
?>