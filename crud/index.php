<?php include "header.php"; ?>

<ul>
	<li><a href="create.php"><strong>Create</strong></a> - add a user</li>
	<li><a href="read.php"><strong>Read</strong></a> - find a user</li>
	<li><a href="login.php"><strong>Login</strong></a> - login a user</li>
	<li><a href="addToppings.php"><strong>Add Toppings</strong></a> - add a topping</li>
	<li><a href="updateToppings.php"><strong>Update Toppings</strong></a> - update a topping</li>
	<li><a href="showOrders.php"><strong>Order List</strong></a> - shows orders</li>
</ul>

<?php include "footer.php"; ?>