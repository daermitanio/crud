<?php 

require "config.php";
require "header.php";
$result = [];
if (isset($_POST['submit'])) {
    try  {

        $connection = new PDO($dsn, $username, $password, $options);

        $sql = "SELECT * 
                        FROM users
                        WHERE email = :email";

        $email = $_POST['email'];

        $statement = $connection->prepare($sql);
        $statement->bindParam(':email', $email, PDO::PARAM_STR);
        $statement->execute();

        $result = $statement->fetchAll(); 
    } catch(PDOException $error) {
        echo $sql . "<br>" . $error->getMessage();
    }

if(md5($_POST['password'])==$result[0]['password']){
    echo "success";
    session_start();
    $_SESSION["user_id"] = $result[0]['user_id'];
    $_SESSION["first_name"] = $result[0]['first_name'];
    $_SESSION["last_name"] = $result[0]['last_name'];
    $_SESSION["address"] = $result[0]['address'];
    $_SESSION["email"] = $result[0]['email'];
    $_SESSION["contact"] = $result[0]['contact'];

    header('Location: menu.php');
    
}else{
    echo "password is incorrect!";
}
}
// var_dump($result);


?>
<form method="post">
    <label for="email">Email Address</label>
    <input type="text" name="email" id="email">
    <label for="pass">Password</label>
    <input type="password" name="password" id="pass">

    <input type="submit" name="submit" value="Login">
</form>

<a href="index.php">Back to Index page</span> </a>

<?php include "footer.php"; ?>