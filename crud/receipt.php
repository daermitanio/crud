<?php require "header.php";
require "model.php";
?>

<?php 
$model = new model;
$total = 0.0;

if(isset($_POST['submit'])){
    $toppings;
    $categories = $model->getData('categories');
    $sizes = $model->getData('sizes');

    foreach($_POST['toppings_id'] as $toppingsId){
        $toppings[]= $model->getToppingsId("toppings",$toppingsId,"topping_id");
    }
    foreach($categories as $category){
        echo($category['category_name'])."<br>";
        foreach($toppings as $topping){
            if($category['category_id'] == $topping[0]['category_id']){
                echo ($topping[0]['name'])."<br>";
                $total = $total + floatval($topping[0]['price']) * floatval($sizes[0]['value']) ;
            }
        }
    }
    echo $total;
}

?>


<?php require "footer.php";?>
