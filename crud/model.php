<?php

class Model{

    function getData($tablename){
        try  {
            require "config.php";
    
            $connection = new PDO($dsn, $username, $password, $options);
    
            $sql = "SELECT * 
                    FROM $tablename";
    
            $statement = $connection->prepare($sql);
            $statement->execute();
    
            $result = $statement->fetchAll();

            return $result;
        } catch(PDOException $error) {
            return $sql . "<br>" . $error->getMessage();
        }
    }

    function getDataById($tablename,$id,$key){
        try  {
            require "config.php";
    
            $connection = new PDO($dsn, $username, $password, $options);
    
            $sql = "SELECT * 
                    FROM $tablename
                    WHERE $key = :id";
                    
    
            $statement = $connection->prepare($sql);
            $statement->bindParam(':id', $id, PDO::PARAM_INT);
            $statement->execute();
    
            $result = $statement->fetchAll();
            return $result;
        } catch(PDOException $error) {
            return $sql . "<br>" . $error->getMessage();
        }
    }

    

    function insertData($tablename,$data){
    
        require "config.php";

        try  {
            $connection = new PDO($dsn, $username, $password, $options);
            

            $sqlForOrderDetails = sprintf(
                    "INSERT INTO %s (%s) values (%s)",
                    "$tablename",
                    implode(", ", array_keys($data)),
                    ":" . implode(", :", array_keys($data))
            );
            // echo $sqlForOrderDetails;
            $statement = $connection->prepare($sqlForOrderDetails);
            $statement->execute($data);

            $last_id = $connection->lastInsertId();

            return $last_id;

        } catch(PDOException $error) {
            echo $sqlForOrderDetails . "<br>" . $error->getMessage();
        }
    
           
    }

    function updatetData($tablename,$data,$id,$key){
    
        require "config.php";

        try  {
            $connection = new PDO($dsn, $username, $password, $options);
            $columns = "";//init a variable to store different columns of table
            foreach($data as $column => $row){
                    $columns .= $column."= :".$column.",";
            }
            $columns .= "/";
            $columns = rtrim($columns,",/");
            // $sql = "UPDATE MyGuests SET lastname='Doe' WHERE id=2";
            $sql= sprintf(
                    "Update %s Set %s Where $key = :$key",
                    "$tablename",
                    $columns
            );
            $statement = $connection->prepare($sql);

            foreach($data as $column => $row){
                if (is_numeric($row)){ //to sort the numeric and non-numeric
                    $statement->bindParam(':'.$column, $row, PDO::PARAM_INT);
                }else{
                    $statement->bindParam(':'.$column, $row, PDO::PARAM_STR);
                }
            }
            $statement->bindParam(':'.$key, $id, PDO::PARAM_INT);
            $data[$key] = $id;
            $statement->execute($data);
            return;

        } catch(PDOException $error) {
            echo $sql . "<br>" . $error->getMessage();
        }
    
           
    }


    function getJoinedTable($orderId){
        try  {
            require "config.php";
    
            $connection = new PDO($dsn, $username, $password, $options);
    
            $sql = "SELECT *
            from order_details
            inner join toppings
            on toppings.topping_id = order_details.topping_id
            where order_id = :order_id";
                    
            $statement = $connection->prepare($sql);
            $statement->bindParam(':order_id', $orderId, PDO::PARAM_INT);
            $statement->execute();
    
            $result = $statement->fetchAll();
            return $result;
        } catch(PDOException $error) {
            return $sql . "<br>" . $error->getMessage();
        }
    }


    function destroyData($tablename,$id,$key){
    
        require "config.php";

        try  {
            $connection = new PDO($dsn, $username, $password, $options);
            
            // $sql = "DELETE FROM MyGuests WHERE id=3";
            $sqlForOrderDetails = sprintf(
                    "DELETE
                    From $tablename
                    Where $key = :id"
            );
            // echo $sqlForOrderDetails;
            $statement = $connection->prepare($sqlForOrderDetails);
            $statement->bindParam(':id', $id, PDO::PARAM_INT);
            $statement->execute();

            return;

        } catch(PDOException $error) {
            echo $sqlForOrderDetails . "<br>" . $error->getMessage();
        }
    
           
    }
}

?>