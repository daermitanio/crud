<?php 
require "header.php";
require "model.php";
require "process.php";
$model = new Model;

$categories = $model->getData("categories");
$toppings = $model->getData("toppings");
?> 

<form action="" method = "POST">
    Size
    <?php  $sizes = $model->getData("sizes");
    
        foreach($sizes as $size){ ?>
        <div>
            <input type="radio" name="sizes" value="<?=$size['size_id']?>"><?=$size['size']?>
        </div>
    <?php }?>
    <?php
    foreach ($categories as $category){
        ?>
        <br><?= $category["category_name"]?>
        <?php
        foreach($toppings as $topping){
            if($category["category_id"] == $topping["category_id"]){?>
            <div>
                <input type = "checkbox" name = "toppings_id[]" value = "<?= $topping["topping_id"]?>"><?= $topping["name"]?>
            </div>
            <?php
            }
        }
    }
    ?>
    </br>
    <input type = "submit" name = "submit" value = "Submit">
</form>

<?php 

    if(isset($_POST["submit"])){
            $process = new process;
            $orderList = $process->computeTotalOrder($_POST);?>
            <h4>Size</h4>
                <div>
                    <?=$orderList["sizes"][0]["size"]?> <?=$orderList["sizes"][0]["value"]?>
                </div>
            <?php
            foreach($categories as $category){?>
                <h4><?= $category["category_name"]?></h4>
                <?php foreach($orderList["toppings"] as $topping){
                    if($category["category_id"] == $topping[0]["category_id"]){?>
                        <div>
                            <?= $topping[0]["name"]?> <?= $topping[0]["price"]?>
                        </div>
                        <?php
                    }
                }         
            }
            ?>
            <h2>Total= <?= $orderList["total"] ?>Php</h2>
            <?php
            
}


require "footer.php";
?>

<a href="index.php"><strong>Back to Index page</strong></a>