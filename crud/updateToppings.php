<?php require "header.php"; ?>
<?php require "model.php"; ?>

<?php 

$model = new model;

if(isset($_POST["delete"])){
    $model->destroyData("toppings",$_POST["topping_id"],"topping_id");
}

$toppings = $model->getData("toppings");
?>
 <table >
    <tr>
        <th>Toppings Name</th>
        <th>Price</th> 
        <th>Action</th>
    </tr>

<?php foreach($toppings as $topping):?>

    <tr>
        <td><?= $topping["name"] ?></td>
        <td><?= $topping["price"] ?></td>
        <td>
            <form action="editToppings.php" method="POST">
                <input value="<?= $topping["topping_id"] ?>" type="hidden" name="topping_id">
                <input value="Update" type="submit" name="update">
            </form>
            <form action="" method="POST">
                <input value="<?= $topping["topping_id"] ?>" type="hidden" name="topping_id">
                <input value="Delete" type="submit" name="delete">
            </form>
        </td>
    </tr>    

<?php endforeach; ?>

<a href="index.php">Back to Index page</a>
<?php require "footer.php"; ?>