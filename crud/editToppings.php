<?php require "header.php";?>
<?php require "model.php";?>

<?php 


$model = new Model;
$categories = $model->getData("categories");
if(isset($_POST["update"])){
    
$topping = $model->getDataById("toppings",$_POST["topping_id"],"topping_id")[0];
}

if(isset($_POST["edit"])){
    $data = [
        "name" => ucfirst($_POST["name"]),
        "price" => $_POST["price"],
        "category_id" => $_POST["category_id"]
    ];
    array_map('ucfirst', $data);
    $model->updatetData("toppings",$data,$_POST["topping_id"],"topping_id");
}
?>

<form action="" method = "POST">
    <input type="hidden" id="topping_id" name="topping_id" value = "<?= $topping["topping_id"]?>" required>
    <label for="name">Name:</label>
    <input type="text" id="name" name="name" value = "<?= $topping["name"]?>" required>
    <label for="price">Price:</label>
    <input type="text" name="price" id="price" value = "<?= $topping["price"]?>" required>
    <label for="categories">Categories</label>
    <select id="categories" name = "category_id">
        <?php  foreach($categories as $category): ?> 
            <?php if($category["category_id"] == $topping["category_id"]):?>
                <option value = "<?= $category["category_id"]?>" selected><?= $category["category_name"]?> </option>
            <?php else: ?>
                <option value = "<?= $category["category_id"]?>"><?= $category["category_name"]?> </option>
            <?php  endif; ?>
        <?php  endforeach; ?>
    </select>
    <br><br>
    <input type = "submit" value = "Edit" name = "edit">
       
</form>

<a href="index.php">Back to Index page</a>

<?php require "footer.php" ?>