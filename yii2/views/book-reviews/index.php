<h1>Book Review List</h1>

<table class="table table-striped">

    <tr>
        <th>Book title</th>
        <th>Author</th>
        <th>Action</th>
    </tr>

    <?php 
    foreach($booksList as $book){
        $id = $book['id'];
    ?>
    <tr>
        <th><?= $book['book_title']?></th>
        <th><?= $book['author']?></th>
        <th><a href="/book-reviews/view?id=<?= $id ?>">
        <span class="glyphicon glyphicon-fire" aria-hidden="true"></span>
        View</th>
    </tr>
    <?php 
    }
    ?>

</table>