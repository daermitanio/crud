<?php

namespace app\controllers;


use Yii;
use yii\web\Controller;

class BookReviewsController extends Controller{


    public function actionIndex(){
        $data['booksList'] = $this->actionGetBooksList();
        return $this->render('index', $data);
    }

    public function actionView($id){
        $data['id'] = $id;

        $booksList = $this->actionGetBooksList();
        foreach($booksList as $book){
            echo "We have book Id " . $book['id'] . "<br>";
        }
        die();


        return $this->render('view', $data);
    }

    public function actionGetBooksList(){
        $booksList = [
            ['id' => 1, 'book_title' => "The Power of Now", 'author' => "Harold", 'amazon_url' => 'http://www.amazon.com'],
            ['id' => 2, 'book_title' => "Edi wow", 'author' => "Eddie", 'amazon_url' => 'http://www.amazon.com'],
            ['id' => 3, 'book_title' => "Php book", 'author' => "Some author of PHP", 'amazon_url' => 'http://www.amazon.com']
        ];

        return $booksList;
    }
  
    // public function actionIndex(){
        
    //     // $data['size', 'topping', 'price'] = 1.2, "Bacon", 60;
    //     $data['name'] = "David";
    //     $data['age'] = 20;
    //     $data['city'] = "Valenzuela";
    //     return $this->render('Hello',$data);
    // }
    // public function actionAnotherPage(){
    //     $this->layout = "alt_layout";
    //     $data['name'] = "David";
    //     $data['age'] = 20;
    //     $data['city'] = "Valenzuela";
    //     return $this->render('Hello',$data);
    // }
}
