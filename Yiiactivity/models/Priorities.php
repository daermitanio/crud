<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;


/**
 * This is the model class for table "priorities".
 *
 * @property int $priority_id
 * @property string $description
 * @property string $hours
 */
class Priorities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'priorities';
    }

    public function behaviors()
    {
        return [
            // Other behaviors
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];   
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['hours'], 'number'],
            [['description'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'priority_id' => 'Priority ID',
            'description' => 'Description',
            'hours' => 'Hours',
        ];
    }
}
