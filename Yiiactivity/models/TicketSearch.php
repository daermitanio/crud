<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tickets;

/**
 * TicketSearch represents the model behind the search form of `app\models\Tickets`.
 */
class TicketSearch extends Tickets
{
    /**
     * {@inheritdoc}
     */

     public $creator;
     public $updateBy;
     public $categoryName;
     public $subCat;
     public $ticketType;
     public $priority;

    public function rules()
    {
        return [
            [['user_id', 'updated_by', 'category_id', 'sub_category_id', 'priority_id'], 'integer'],
            [['creator','updateBy', 'subCat' , 'ticketType', 'categoryName','priority','ticket_type', 'description', 'notes', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tickets::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query->joinWith([
            "users",
            "updatedBy",
            "priorityDes",
            "category"
            ]); // join

        // grid filtering conditions
        $query->andFilterWhere([
            'tikcket_id' => $this->tikcket_id,
            'user_id' => $this->user_id,
            'updated_by' => $this->updated_by,
            'category_id' => $this->category_id,
            'sub_category_id' => $this->sub_category_id,
            'priority_id' => $this->priority_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'ticket_type', $this->ticket_type])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'users.first_name', $this->creator])
            ->andFilterWhere(['like', 'description', $this->subCat])
            ->andFilterWhere(['like', 'ticket_type', $this->ticketType])
            ->andFilterWhere(['like', 'categories.name', $this->categoryName])
            ->andFilterWhere(['like', 'priorities.description', $this->priority])
            ->andFilterWhere(['like', 'u2.last_name', $this->updateBy]);

        return $dataProvider;
    }
}
