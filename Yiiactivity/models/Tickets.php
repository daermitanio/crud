<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "tickets".
 *
 * @property string $tikcket_id
 * @property string $user_id The user_id of the requestor
 * @property string $updated_by user_id of the last user who updated the ticket
 * @property string $category_id Foreign key to the categories table
 * @property string $sub_category_id Foreign key to sub_categories, list them down based on ticket_type selected
 * @property string $ticket_type Enum type to be used to get the sub_categories (tasks) for the ticket
 * @property string $priority_id
 * @property string $description
 * @property string $notes
 * @property string $created_at
 * @property string $updated_at
 */
class Tickets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tickets';
    }


    public function behaviors()
    {
        return [
            // Other behaviors
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];   
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'updated_by', 'category_id', 'sub_category_id', 'priority_id'], 'integer'],
            [['ticket_type', 'notes'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['description'], 'string', 'max' => 250],
            [['user_id', 'updated_by', 'category_id', 'sub_category_id','ticket_type','priority_id'],'required'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'tikcket_id' => '#',
            'user_id' => 'Created By',//user_id
            'updated_by' => 'Updated By',//user_id
            'category_id' => 'Category',//category_id
            'sub_category_id' => 'Sub Category',//sub_category_id
            'ticket_type' => 'Ticket Type',
            'priority_id' => 'Priority',
            'description' => 'Description',
            'notes' => 'Notes',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getUsers(){
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);//gets the creator this ticket from models/users class
    }
    public function getCreator(){
        return $this->users->first_name;
    }
    public function getUpdatedBy(){
        return $this->hasOne(Users::className(), ['user_id' => 'updated_by'])->from(Users::tableName() . ' u2');
    }
    public function getUpdateBy(){
        return $this->updatedBy->last_name;
    }
    public function getSubCategory(){
        return $this->hasOne(Sub_categories::className(), ['sub_category_id' => 'sub_category_id']);
    }
    public function getSubCat(){
        return $this->subCategory->description;
    }
    public function getCategory(){
        return $this->hasOne(Categories::className(), ['category_id' => 'category_id']);
    }
    public function getCategoryName(){
        return $this->category->name;
    }
    public function getTicketType(){
        return $this->subCategory->ticket_type;
    }
    public function getPriorityDes(){
        return $this->hasOne(Priorities::className(),['priority_id' => 'priority_id']);
    }
    public function getPriority(){
        return $this->priorityDes->description;
    }
}
