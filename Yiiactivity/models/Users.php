<?php

namespace app\models;

use Yii;
use \yii\db\ActiveRecord;
use \yii\web\IdentityInterface;

/** 
 * This is the model class for table "users".
 *
 * @property string $user_id
 * @property string $email
 * @property string $password
 * @property string $first_name
 * @property string $last_name
 * @property string $created_at
 * @property string $updated_at
 */
class Users extends ActiveRecord implements IdentityInterface
{
    public $hashPassword = false;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'safe'],
            [['email', 'password', 'first_name', 'last_name'], 'string', 'max' => 200],
            [['email'], 'unique'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'email' => 'Email',
            'password' => 'Password',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    
    public static function findIdentity($user_id)
    {
        return static::findOne($user_id);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getId()
    {
        return $this->user_id;
    }

    public function getAuthKey()
    {
        // return $this->authKey;
        return "12232h3j1g3yu123guyj1";
    }

    public function validateAuthKey($authKey)
    {
        // return $this->getAuthKey() === $authKey;
        return true;
    }

    
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password);
    }

    public function beforeSave($insert)
    {
        // if (parent::beforeSave($insert)) {
        //     if ($this->hashPassword) {
                $this->password = \Yii::$app->security->generatePasswordHash($this->password, 10);
            // }
            return true;
        // } else {
            // return false;
        // }
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $auth = Yii::$app->authManager;
            $role = $auth->getRole('member');
            $auth->assign($role, $this->id);
        }        
        parent::afterSave($insert, $changedAttributes);
    }
}
