<?php

namespace app\commands;

use yii\console\Controller;
use app\models\Users;
use \Yii;
use app\rbac\ProfileRule;

class UserController extends Controller
{
    public function actionLoadUsers()
    {

        Users::deleteAll();

        $UserData = [
            [
                'email' => 'dae@gmail.com',
                'password' => '12345',
                'first_name' => 'David',
                'last_name' => 'Ermitanio',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'email' => 'tester@gmail.com',
                'password' => '12345',
                'first_name' => 'Test',
                'last_name' => 'Er',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ],
            [
                'email' => 'admin@gmail.com',
                'password' => '12345',
                'first_name' => 'admin',    
                'last_name' => 'admin',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]
        ];

        foreach ($UserData as $data) {
            $User = new Users($data);
            $User->hashPassword = true;
            $User->save();
        }
    }


    public function actionPermissions()
    {
        $auth = Yii::$app->authManager;

        $updateUser = $auth->createPermission('updateUsers');
        $updateUser->description = 'Update a monster';
        $auth->add($updateUser);

        $deleteUser = $auth->createPermission('deleteUsers');
        $deleteUser->description = 'Delete a monster';
        $auth->add($deleteUser);
    }

    public function actionRoles()
    {
        $auth = Yii::$app->authManager;

        $updateUser = $auth->getPermission('updateUser');
        $deleteUser = $auth->getPermission(('deleteUser'));

        $member = $auth->createRole('member');
        $auth->add($member);
        $auth->addChild($member, $updateUser);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $deleteUser);
        $auth->addChild($admin, $member);

    }

    public function actionRules()
    {
        $auth = Yii::$app->authManager;

        $rule = new ProfileRule();
        $auth->add($rule);

        $updateUser = $auth->getPermission('updateUser');
        $updateUser->ruleName = $rule->name;
        $auth->update('updateUser', $updateUser);
    }
}
