<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\sidenav\SideNav;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode("Ticketing Tool") ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout ('.ucfirst(Yii::$app->user->identity->last_name) . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);

    echo SideNav::widget(
        [
            
            'items' => [

                [
                    'label' => 'Tickets',
                    'visible' => !Yii::$app->user->isGuest,
                    'icon'=> 'qrcode',
                    'items' => [
                        ['label' => 'Create new Ticket', 'icon'=>'check', 'url' => ['/ticket/create']],
                        ['label' => 'Manage Tickets', 'icon'=>'edit','url' => ['/ticket/index']],
                        ['label' => 'My Tickets', 'icon'=>'edit','url' => ['/ticket/my-tickets']],
                    ],
                ],

                [
                    'label' => 'Categories',
                    'visible' => !Yii::$app->user->isGuest,
                    'icon'=> 'file',
                    'items' => [
                        ['label' => 'Add Category', 'icon'=>'check', 'url' => ['/category/create']],
                        ['label' => 'Manage Categorys', 'icon'=>'edit','url' => ['/category/index']],
                    ],
                ],


                
                [
                    'label' => 'Sub-category',
                    'url' => ['/sub-category'],
                    'visible' => !Yii::$app->user->isGuest,
                    'icon'=> 'duplicate',
                    'items' => [
                        ['label' => 'Add Sub-Category', 'icon'=>'check', 'url' => ['/sub-category/create'], 'class'=>'active'],
                        ['label' => 'Manage Sub-category', 'icon'=>'edit','url' => ['/sub-category/index']],
                    ],
                ],

                [
                    'label' => 'My Profile',
                    'url' => ['/user/view','id' => Yii::$app->user->id],
                    'visible' => !Yii::$app->user->isGuest,
                    'icon'=> 'user'
                ]

            ]

        ]);
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
   
</div>



<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
