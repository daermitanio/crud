<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categories;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Sub_categories */
/* @var $form yii\widgets\ActiveForm */

$categoriesMap = ArrayHelper::map(Categories::find()->all(), 'category_id','name');

?>

<div class="sub-categories-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')->dropDownList($categoriesMap) ?>

    <?= $form->field($model, 'ticket_type')->dropDownList([ 'incident' => 'Incident', 'problem' => 'Problem', 'service_request' => 'Service request', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_at')->textInput(['type' => 'hidden' ,'value' => date('Y-m-d H:i:s')]) ?>

    <?= $form->field($model, 'updated_at')->textInput(['type' => 'hidden' ,'value' => date('Y-m-d H:i:s')]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
