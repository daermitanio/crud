<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categories;
use app\models\Sub_categories;
use app\models\Priorities;
use app\models\Users;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Tickets */
/* @var $form yii\widgets\ActiveForm */

$dataCategory=ArrayHelper::map(Categories::find()->asArray()->all(), 'category_id', 'name');
$dataPriorities = ArrayHelper::map(Priorities::find()->asArray()->all(),'priority_id','description');

// var_export(Users::findIdentity($model->user_id));die;
// $dataSubCategory=ArrayHelper::map(Sub_categories::find()->asArray()->all(), 'sub_category_id', 'description');
?>

<div class="tickets-form">

  

    <?php if($action == "update"):?>
        <?php $form = ActiveForm::begin(); ?>
            <?= $form->field($model, 'user_id')->textInput(['maxlength' => true, 'type' => 'hidden']) ?>
            <input class = "form-control" id = "created_by" disabled value ="<?= Users::findIdentity($model->user_id)->last_name; ?>" >
            <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true,'value' => Yii::$app->user->identity->user_id,'type' => 'hidden']) ?>
            <input class = "form-control" id = "updated_by" disabled value ="<?= Yii::$app->user->identity->last_name?>" >
            <?= $form->field($model, 'category_id')->dropDownList($dataCategory, 
                [
                    'prompt'=>'-Choose a Category-',
                    'onchange'=>'
                        $.post( "'.Yii::$app->urlManager->createUrl('sub-category/lists?id=').'"+$(this).val(), function( data ) {
                            $( "select#sub_category_id" ).html( data );
                            $( "#sub_category_id" ).removeAttr("disabled")
                        });
                ']); ?>
            <?= $form->field($model, 'sub_category_id')->dropDownList([],           
                [
                    'id'=>'sub_category_id',
                    'disabled'=>'true'
                ]
            ); ?>

            <?= $form->field($model, 'ticket_type')->
                dropDownList([ 'incident' => 'Incident', 'problem' => 'Problem', 'service_request' => 'Service request', ],
                ['prompt'=>'-Choose a Ticket Type-',]) 
            ?>

            <?= $form->field($model, 'priority_id')->dropDownList($dataPriorities,
                ['prompt'=>'-Choose a Priority Level-',
                    'onchange'=>'
                    try{
                        $.get( "'.Yii::$app->urlManager->createUrl('ticket/get-priority?id=').'"+$(this).val(), 
                        function( data ) {
                            $("#priority_hours").val( data );
                        });
                    }
                    catch(err) {
                        console.log(err);
                    }
                    '
                ]
                // $( "#priority_hours" ).val(data);
            ); ?>
            
            <input type="text" class="form-control" disabled id="priority_hours">

            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

        <?php ActiveForm::end(); ?>
    <?php  else:?>
        
    <?php endif;?>

</div>
