<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Categories;
use app\models\Sub_categories;
use app\models\Priorities;
use app\models\Users;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Tickets */
/* @var $form yii\widgets\ActiveForm */

$dataCategory=ArrayHelper::map(Categories::find()->asArray()->all(), 'category_id', 'name');
$dataPriorities = ArrayHelper::map(Priorities::find()->asArray()->all(),'priority_id','description');

$this->title = 'Create Tickets';
$this->params['breadcrumbs'][] = ['label' => 'Tickets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tickets-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin(); ?>

        <div class="col-lg-6">
            <?= $form->field($model, 'user_id')->textInput(['maxlength' => true, 'value' => Yii::$app->user->identity->user_id,'type' => 'hidden']) ?>
            <input class = "form-control" id = "created_by" disabled value ="<?= Yii::$app->user->identity->last_name ?>" >
        </div>
        <div class="col-lg-6">
        <?= $form->field($model, 'updated_by')->textInput(['maxlength' => true,'value' => Yii::$app->user->identity->user_id,'type' => 'hidden']) ?>
            <input class = "form-control" id = "updated_by" disabled value ="<?= Yii::$app->user->identity->last_name?>" > 
        </div>
        <div class="col-lg-3">
            <?= $form->field($model, 'category_id')->dropDownList($dataCategory, 
            [
                'prompt'=>'-Choose a Category-',
                'onchange'=>'
                    $.post( "'.Yii::$app->urlManager->createUrl('sub-category/lists?id=').'"+$(this).val(), function( data ) {
                        $( "select#sub_category_id" ).html( data );
                        $( "#sub_category_id" ).removeAttr("disabled")
                    });
                '
            ]); ?>
        </div>
        <div class="col-lg-3">
        <?= $form->field($model, 'sub_category_id')->dropDownList([],           
                [
                    'id'=>'sub_category_id',
                    'disabled'=>'true'
                ]
            ); ?>
        </div>
        
        
        <div class="col-lg-3">
        <?= $form->field($model, 'priority_id')->dropDownList($dataPriorities,
            ['prompt'=>'-Choose a Priority Level-',
                'onchange'=>'
                try{
                    $.get( "'.Yii::$app->urlManager->createUrl('ticket/get-priority?id=').'"+$(this).val(), 
                    function( data ) {
                        $("#priority_hours").val( data );
                    });
                }
                catch(err) {
                    console.log(err);
                }
                '
            ]
            // $( "#priority_hours" ).val(data);
        ); ?>
        </div>
        <div class="col-lg-3">
            <label class="control-label">Hours:</label>
            <input type="text" class="form-control" disabled id="priority_hours">
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'ticket_type')->
                dropDownList([ 'incident' => 'Incident', 'problem' => 'Problem', 'service_request' => 'Service request', ],
                ['prompt'=>'-Choose a Ticket Type-',]) 
            ?>
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-12">
            <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>
        </div>
        
        <div class="form-group col-lg-12">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

    <?php ActiveForm::end(); ?>

</div>
