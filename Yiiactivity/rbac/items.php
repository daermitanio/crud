<?php
return [
    'updateUsers' => [
        'type' => 2,
        'description' => 'Update a monster',
    ],
    'deleteUsers' => [
        'type' => 2,
        'description' => 'Delete a monster',
    ],
    'member' => [
        'type' => 1,
    ],
];
